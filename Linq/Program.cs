﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq
{
    class Program
    {
        static void Main(string[] args)
        {

            List<Persona> personas = new List<Persona>();
            personas.Add(new Persona()
            {
                Nombre = "Alvaro",
                Edad = 24,
                Direcciones = new List<Direccion>()
            {
                { new Direccion() { Calle = "Calle falsa 1", Ciudad = "Ciudad 1", CodigoPostal = 200 } } ,
                { new Direccion() { Calle = "Calle falsa 3", Ciudad = "Ciudad 2", CodigoPostal = 200 } }
            }
            });

            personas.Add(new Persona() { Nombre = "Sandra", Edad = 25, Direcciones = new List<Direccion>() { new Direccion() { Calle = "Calle falsa 2", Ciudad = "Ciudad 2", CodigoPostal = 200 } } });
            personas.Add(new Persona() { Nombre = "David", Edad = 23, Direcciones =new List<Direccion>() });
            personas.Add(new Persona() { Nombre = "Santi", Edad = 23, Direcciones = new List<Direccion>() });

            //Linq es una lista de metodos de Extension, que al ponerlos con el using dentro de la clase
            //Todas las clases que los pueden llevar los obtienen, las listas en este caso ganan un gran poder en funciones
            //Linq como this tiene una interfaz  que implementan muchos objetos  de colecciones
            //A la vez los metodos de extension son funciones Lamda osea metodos funcionales

            /**** WHERE ****/
            //Where es un condicional que nos devuelve los elementos que cumplan true en la condicion logica
            personas.Where(x => x.Nombre.Equals("Alvaro")).ToList();


            /**** SELECT ***/
            //Sirve para convertir u ntipo de lista en otro tipo de lista
            List<String> nombres = new List<String>();
            List<Tuple<String, int, String, String>> tuplas = new List<Tuple<String, int, String, String>>();

            nombres = personas.Select(x => x.Nombre).ToList();
            //tuplas = personas.Select(x => new Tuple<String, int, String, String>(x.Nombre, x.Edad, x.Direcciones[0].Calle, x.Direcciones[0].Ciudad)).ToList();

            /*** SELECTMANY ****/

            //Con el many si intentamos acceder a una coleccion nos dara una lista de todas las colecciones, 
            //con el select nos dara una lista, con todas las direcciones que hay dentro de los objetos de esa lista
            //var calles = personas.SelectMany(x => x.Direcciones, (persona, Direcciones) => Direcciones).Select(x => x).GroupBy(x => x.Ciudad);

            var calles = personas.SelectMany(x => x.Direcciones, (persona, Direcciones) => new { direcciones = Direcciones, persone = persona.Nombre }).ToList();

            //string ccalle1 = calles[0].calle;
            //foreach (igrouping<string, direccion> tree in calles)
            //{
            //    console.writeline("selectmany: " + tree.key + " - " + tree.select(x => x.calle).tolist()[0]);
            //    console.writeline("selectmany: " + tree.key + " - " + tree.select(x => x.calle).tolist()[1]);
            //    console.writeline("selectmany: " + tree.key + " - " + tree.select(x => x.calle).tolist()[2]);
            //}

            //Console.WriteLine("SELECTMANY: " + calles[1].Nombre);
            //Console.WriteLine("SELECTMANY: " + calles[2].Nombre);

            var ciudades = personas.Select(x => x.Direcciones).Select(x => x).ToList();
            String ccalle2 =  ciudades[0][0].Calle;
            Console.WriteLine("SELECT: " + ciudades[1].Count);

            /** SELECT ANY ***/
            //Comrpueba si tiene un elemento
            var z = personas.SelectMany(x => x.Direcciones).Any(x =>x.Ciudad.Equals("Madrid"));
            //Te devueklve los objetos que lo cumplan
            var xz = personas.SelectMany(x => x.Direcciones).Where(x => x.Ciudad.Equals("Madrid")).ToList();

            //Si no se encuntra un resultado posible causa excepcion.
            var perFirst = personas.SelectMany(x => x.Direcciones).First(x => x.Ciudad.Equals("Madrid"));
            //N ocausa excepcion
            var perFirstDef = personas.SelectMany(x => x.Direcciones).FirstOrDefault(x => x.Ciudad.Equals("Madrid"));

            //Si no se encuntra un resultado posible causa excepcion.
            var perLast = personas.SelectMany(x => x.Direcciones).Last(x => x.Ciudad.Equals("Madrid"));
            //N ocausa excepcion
            var perLastDef = personas.SelectMany(x => x.Direcciones).LastOrDefault(x => x.Ciudad.Equals("Madrid"));


            var DirTop10 = personas.SelectMany(x => x.Direcciones).Take(10).ToList();

            var Dir10To20 = personas.SelectMany(x => x.Direcciones).Skip(10).Take(10);

            var DirTop10Order = personas.OrderBy(x => x.Edad).Take(10).ToList();

            var DirTop10OrderDesc = personas.OrderByDescending(x => x.Edad).Take(10).ToList();

            //Crear una nueva ordenacion, ordenar por 

            var dictonaryGroup = personas.GroupBy(x => x.Direcciones).ToDictionary(x => x.Key);

            var array = personas.OrderBy(x => x.Edad).Take(10).ToArray();

            //Casteas la coleccion a otra posible
            var castList = personas.OfType<Interfaz>().ToList();

            Console.ReadKey();








            List<Persona> alumnos = new List<Persona>();
            alumnos.Add(new Persona()
            {
                Nombre = "Alvaro",
                Edad = 24,
                asiganturas = new List<Asignatura>()
                        {
                            { new Asignatura()
                            {

                                Nombre = "Lengua",
                                Examenes = new List<Examen>()
                                {
                                    { new Examen()
                                        {
                                            fecha = new DateTime(),
                                            Nota = 10
                                        }
                                    },
                                    { new Examen()
                                        {
                                            fecha = new DateTime(),
                                            Nota = 10
                                        }
                                    },
                                    { new Examen()
                                        {
                                            fecha = new DateTime(),
                                            Nota = 10
                                        }
                                    }

                            }   }
                            }
                        }
                }
            );

            var agruparAsignaturas = alumnos.SelectMany(
                estudiante => estudiante.asiganturas,
                //Nos devuelve un Enumeration <Alumno, Asignaturas>
                //Con el cual creamos objetos que tienen solo la Asignatura y el Nombre
                (Alumno, Asignatura) => new { asignatura = Asignatura.Nombre, examenes = Asignatura.Examenes })
                .GroupBy(asig => asig.asignatura);
            

            var mediaAsignaturas = agruparAsignaturas.Select(
                asigGroup => new
                {
                    Subject = asigGroup.Key,
                    Media = asigGroup.SelectMany(asig => asig.examenes).Average(t => t.Nota)

                }
                );

        }
    }
}

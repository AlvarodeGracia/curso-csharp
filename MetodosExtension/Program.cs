﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosExtension
{

    class Program
    {
        static void Main(string[] args)
        {
  
            Dictionary<String, String> dictionary = new Dictionary<string, string>();

            var elements = new List<KeyValuePair<String, String>>();
            elements.Add(new KeyValuePair<string, string>("clave1", "Valor 1"));
            elements.Add(new KeyValuePair<string, string>("clave2", "Valor 2"));

            //Metodo normal
            Utils.addRange<String, String>(dictionary, elements);

            //Metodo de Extension
            //Al haber creado ese metodo en util de esa forma añadiendo el this, este metodo
            //se convierte en metodo de extension, lo que significa que todos los objetos que
            //hayan sido indicados por el this en este caso los Dictionary obtendras ese metodo como suyo
            //y como veis no hace falta pasarselo como parametro ya que el mismo ya se incluye co nel this.
            dictionary.addRangeExtension<String, String>(elements);
        }
    }
}

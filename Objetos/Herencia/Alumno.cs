﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objetos
{
    class Alumno: Persona
    {
        public int numAsignaturas { get; set; }

        //override indicamos que vamos a sobreescribir el metodo
        public override void saludar()
        {
            //base es el objeto padre
            base.saludar();
            Console.Write( $"Soy un alumno");
        }

        //Si no sobreescrives este metodo el equals lo hace con los hash Posiciones de memoria
        public override bool Equals(object obj)
        {
            var alumnoObj = (Alumno)obj;
            if (alumnoObj.Nombre.Equals(this.Nombre)){
                return true;
            }
                
            return false;
        }

        //No hacerle Override a este metodo
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }


    }
}

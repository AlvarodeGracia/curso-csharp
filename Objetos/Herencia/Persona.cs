﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objetos
{
    class Persona
    {

        protected String Nombre { get; set; }
        protected int Edad { get; set; }

        //Permitimos con virtual sobreescribir el metodo
        public virtual void saludar()
        {
            Console.Write($"Hola soy {Nombre}");
        }

    }
}

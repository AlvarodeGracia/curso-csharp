﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroduccionLenguaje
{
    class Program
    {
        static void Main(string[] args)
        {

            int var1 = 3;
            int var2 = 6;
            int var3 = var1 + var1;

            int[] array = { 1, 2, 3, 4, 5, 6, 7 };
            //Condicionales
            if(var3 > 5)
            {
                //Codigo
            }
            else if(var3 > 10)
            {
                //codigo  
            }
            else
            {
                //codigo
            }

            switch (var3)
            {
                case 1:
                    //codeM;
                    break;
                case 2:
                    //code
                    break;
                default:
                    break;

            }

            //Bucles
            for (int i = 0; i< 19; i++)
            {

            }

            while(var3 > 5)
            {
                var3++;
            }

            foreach(int x in array)
            {
                //code
            }


        }
    }
}

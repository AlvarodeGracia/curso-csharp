﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosExtension
{
    public static class Utils
    {

        public static void addRange<K, V>(Dictionary<K, V> dictionary, List<KeyValuePair<K, V>> elements)
        {
            foreach (var keyValue in elements)
            {
                dictionary.Add(keyValue.Key, keyValue.Value);

            }
        }

        public static void addRangeExtension<K, V>(this Dictionary<K, V> dictionary, List<KeyValuePair<K, V>> elements)
        {
            foreach (var keyValue in elements)
            {
                dictionary.Add(keyValue.Key, keyValue.Value);

            }
        }
    }
}

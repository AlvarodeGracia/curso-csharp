﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colecciones
{
    class Program
    {
        static void Main(string[] args)
        {
            /***** LISTAS ****/
            //Array list es de objetos genericos para usar los elementos 
            //que aqu ise guardan hay que castearlos primero para saber de que tipo son
            //No se usa mucho
            ArrayList array = new ArrayList();

            //Esta lista es mucho mas explicita al objeto que lleva dentro 
           //Y es mucho mas facil de 
            List<String> lista = new List<String>();

            //Agregar elemento a la coleccion
            lista.Add("Texto");
            //Cantidad de objetos en la coleccion
            lista.Count();

            //Recogemos el primer elemento
            String aux = lista[0];


            /**** PILAS ****/
            //El ultimo que entra el primero que sale
            Stack<String> stack = new Stack<String>();
            stack.Push("Ultimo en salir");
            stack.Push("Segundo en Salir");
            stack.Push("Primero en Salir");

            //Scara elemento
            String PilaPrimero = stack.Pop();


            /**** COLAS ****/
            //El primero que entra es el primero que sale
            Queue<String> queue = new Queue<String>();
            queue.Enqueue("Primero en salir");
            queue.Enqueue("Segundo en salir");
            queue.Enqueue("Ultimo en salir");

            String colaPrimero = queue.Dequeue();


            /**** DICIONARIO ****/
            //Este tip ode coleccion es de clave valor, las claves noo se pueden repetir
            Dictionary<String, String> dictionary = new Dictionary<string, string>();

            //Agregar elementos a la coleccion, n ose puede repetir la clave
            dictionary.Add("clave1", "Valor 1");
            dictionary.Add("clave2", "Valor 2");
            dictionary.Add("clave3", "Valor 3");
            dictionary.Add("clave4", "Valor 4");

            //Cambiar valor
            dictionary["clave4"] = "Otro Valor";

            //Para obtener el valor de una clave
            String valor3 = dictionary["clave3"];
            

            //para ver si contiene cierta clave
            if (dictionary.ContainsKey("clave4"))
            {
                //Code
            }

            if(dictionary.ContainsValue("Valor 1"))
            {
                //Code
            }

            string outValor;
            
            //Este metodo nos permite obtener como return mas de un valor
            //Como return del metodo un bool de si ese elemento se encuntra o no
            //Y como out, palabra clave para desiganr que el valor se pasa como referencia
            bool existe = dictionary.TryGetValue("clave2", out outValor);

            foreach(KeyValuePair<String, String> keyValue in dictionary)
            {
                String key = keyValue.Key;
                String value = keyValue.Value;
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objetos
{
    public struct ShelvePosition
    {
        public ShelvePosition(int row, int column)
        {
            this.Row = row;
            this.Column = column;

        }

        public int Row { get; set; }
        public int Column{ get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objetos.Genericos
{
    //Esta interfa es generica asi podemos hacer que esta interfaz sea colocada en cualquier sitio
    //I el metodo implementado sea mas personalziable a la clase
    //Poner restyrinciones con el where -> que sea una clase - class - que tenga u nconstructor - new() - que sea de un tip ode clase - interfaz .
    //new() permite iniciar el objr¡eto T dentor de la app T myT = new T();
    interface IOrdenable<T> where T :  class , Interfaz, new()
    {
        SortedSet<T> ordenar(List<T> list);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq
{
    class Persona : Interfaz
    {

        public String Nombre { get; set; }
        public int Edad { get; set; }

        public List<Direccion> Direcciones { get; set; }

        public List<Asignatura> asiganturas;

    }
}

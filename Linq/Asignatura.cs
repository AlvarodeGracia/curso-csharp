﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq
{
    class Asignatura
    {
        public String Nombre { get; set; }
        public List<Examen> Examenes { get; set; }
    }
}

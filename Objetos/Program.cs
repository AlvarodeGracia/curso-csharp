﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objetos
{
    class Program
    {
        static void Main(string[] args)
        {

            Song song = new Song();
            song.Title = "Titulo";
            song.Seconds = 125;
            song.Position = new ShelvePosition(3, 5);
            song.Play();

            Console.ReadKey();
        }
    }
}
